module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.initConfig({
        concat:{
            dist: {
                src: ['src/order-service.module.js','src/*.js'],
                dest: 'dist/order-service-angularjs-sdk.js'
            }
        }
    });
    grunt.registerTask('default', ['concat']);
};